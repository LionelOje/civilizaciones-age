import React from 'react';
import './button-close-undo.css'


//Este boton lo utilizo en el componente civilization y en el componente modal y me sireve para volver a la vista anterior 

const ButtonCloseUndo = ({Action}) => {
    return (
      <>
        <div className="container" onClick={()=> {Action()}} >
          <div className=" row">
            <div className="col-3 col-sm-6 col-md-5 position-absolute ">  
              <img className="col-6 col-sm-1 col-md-1 size-pergamino-overview  mt-3 position-mobil"  src="./icon-undo.png" ></img>
            </div>
          </div>                                                              
       </div>
      </>
    );
  }
  
  export default ButtonCloseUndo;