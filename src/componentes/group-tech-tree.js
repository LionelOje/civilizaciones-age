import React, {useContext} from 'react';
import './group-tech-tree.css'
import TechTree from './tech-tree';
import {ContextModalCiviliztions} from './modal-civilizations'

// Este componente me contiene y mapea todos las imagenes del arbol technologico de las civilizaciones y se ubica dentro del componente modal. Para ello utiliza el componente tech-tree.

const GroupTechTree = () => {

  const {DisplayTechTrees} = useContext(ContextModalCiviliztions)

  const CardDatos = [
    {
      id:1,
      ImgTech:'./arbol-ingleses.jpg',
      DisplayTechtree: DisplayTechTrees[1]    
    },
    {
      id:2,
      ImgTech:'./arbol-bizantinos.jpg',
      DisplayTechtree: DisplayTechTrees[2]    
    },
    {
      id:3,
      ImgTech:'./arbol-celtas.jpg',
      DisplayTechtree: DisplayTechTrees[3]    
    },
    {
      id:4,
      ImgTech:'./arbol-chinos.jpg',
      DisplayTechtree: DisplayTechTrees[4]    
    },
    {
      id:5,
      ImgTech:'./arbol-francos.jpg',
      DisplayTechtree: DisplayTechTrees[5]    
    },
    {
      id:6,
      ImgTech:'./arbol-godos.jpg',
      DisplayTechtree: DisplayTechTrees[6]    
    },
    {
      id:7,
      ImgTech:'./arbol-japoneses.jpg',
      DisplayTechtree: DisplayTechTrees[7]    
    },
    {
      id:8,
      ImgTech:'./arbol-mongoles.jpg',
      DisplayTechtree: DisplayTechTrees[8]    
    },
    {
      id:9,
      ImgTech:'./arbol-serracenos.jpg',
      DisplayTechtree: DisplayTechTrees[9]    
    },
    {
      id:10,
      ImgTech:'./arbol-persas.jpg',
      DisplayTechtree: DisplayTechTrees[10]    
    },
    {
      id:11,
      ImgTech:'./arbol-teutones.jpg',
      DisplayTechtree: DisplayTechTrees[11]    
    },
    {
      id:12,
      ImgTech:'./arbol-turcos.jpg',
      DisplayTechtree: DisplayTechTrees[12]    
    },
    {
      id:13,
      ImgTech:'./arbol-vikingos.jpg',
      DisplayTechtree: DisplayTechTrees[13]    
    },
    {
      id:14,
      ImgTech:'./arbol-aztecas.jpg',
      DisplayTechtree: DisplayTechTrees[14]    
    },
    {
      id:15,
      ImgTech:'./arbol-hunos.jpg',
      DisplayTechtree: DisplayTechTrees[15]    
    },
    {
      id:16,
      ImgTech:'./arbol-koreanos.jpg',
      DisplayTechtree: DisplayTechTrees[16]    
    },
    {
      id:17,
      ImgTech:'./arbol-mayas.jpg',
      DisplayTechtree: DisplayTechTrees[17]    
    },
    {
      id:18,
      ImgTech:'./arbol-españoles.jpg',
      DisplayTechtree: DisplayTechTrees[18]    
    },
   
  ]

  return (
      <>
        <div className="container">
          <div className=" row justify-content-center">
            <div className="col-10 col-md-10 scroll-tech-tree">
              <div className="scroll-size-height">     
            { 
             CardDatos.map(({ id, ImgTech, DisplayTechtree }) => <TechTree key={id} ImgTech={ImgTech} DisplayTechtree={DisplayTechtree}/>)
            }
              </div>
            </div>
          </div>                                                              
       </div>
      </>
    );
  }
  
  export default GroupTechTree;

  
