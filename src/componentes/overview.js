import React from 'react';
import GroupDatesOverview from './group-dates-overview';
import GroupDescriptionOverview from './group-description-overview';
import './overview.css'


//Este componente esta dentro del componente modal, y me contiene los dos contenedores de los dates-overview (pestaña datos del nav) y dates-description (pestaña historia del nav). Los puse juntos aca por que usan el mismo fondo, entonces lo que hago es ir cambiando el contenido que esta dentro del fondo. 

const Overview = ({DisplayOverview}) => {

  
    return (
      <>
        <div className="container re" style={{display: DisplayOverview}}>
          <div className=" row justify-content-center">
            <div className="col-10 col-md-10">  
              <img className="size-pergamino-overview" src="./pergamino-overwiev.jpg"></img>
            </div>
            <GroupDatesOverview/>
            <GroupDescriptionOverview/>
            
            
          </div>                                                              
       </div>
      </>
    );
  }
  
  export default Overview;

