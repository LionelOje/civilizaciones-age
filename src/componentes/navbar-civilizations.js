import React, {useContext} from 'react';
import './navbar-civilizations.css'
import {ContextCivilitations} from './civilitations'
import {ContextModalCiviliztions} from './modal-civilizations'

//Este componente me contiene el nav que tiene 3 pestañas, las cuales cada una me va mostrando distintos rasgos de la civilizacion seleccionada 

const NavbarCivilizations = ({setDisplayDescriptions, setDisplayTechTrees, setDisplayDates, setDisplayContainerGroupDescription, setDisplayContainerGroupDates, seteffectDescriptionAndDates, setDisplayOverview, seteffectTechTree}) => {

  const {DisplayDates, DisplayTechTrees, DisplayDescriptions} = useContext(ContextModalCiviliztions)

  
  const {FlagsActivCivilization} = useContext(ContextCivilitations)




 /////////////////////////////////////////////////////////////Funcionalidad del nav del modal 


  // Este boton me abre la pestaña seleccionada y me cierra las que no uso por medio de una bandera y un for para ver cual bandera esta en true, osea cual civilizacion esta seleccionada 

  const ButtonOpenSectionAndClose = ( valueDisplay1, valueDisplaycontainer1, valueDisplayOverview, valueDisplayDescription, valueDisplaycontainerDescription, valueDisplay3, valueEffectTechTree) =>{

    
    for (let i=1; i<=18; i++){
      if (FlagsActivCivilization[i]===true){
          setDisplayDates({...DisplayDates, [i]:valueDisplay1})
          setDisplayContainerGroupDates(valueDisplaycontainer1)
          seteffectDescriptionAndDates('effect-description')
          setDisplayOverview(valueDisplayOverview) 
          setDisplayDescriptions({...DisplayDescriptions, [i]:valueDisplayDescription})
          setDisplayContainerGroupDescription(valueDisplaycontainerDescription)
          setDisplayTechTrees({...DisplayTechTrees, [i]:valueDisplay3})
          seteffectTechTree(valueEffectTechTree)
           
      }
    }
  }
  

  //Cada una de estas tres funciones corresponden a cada pestaña del nav. Lo que hacen es llamar a la funcion ButtonOpenSectionAndClose, y le paso los parametros para que me quede unicamente en pantalla la seccion de tech tre, y que me ademas me ejecute el efecto al aparacer. Tambien me pone los valores que interefieren en la visualisacion de las otras dos pestañas en por defecto

  const ButtonNavTechTree = () => {
   
    return ButtonOpenSectionAndClose( 'none','none', 'none', 'none', 'none','block','effectTechTree');
  }

  const ButtonNavDates = () => {
   
    return ButtonOpenSectionAndClose( 'block','block', 'block', 'none', 'none', 'none', null);
  }

  const ButtonNavHistory = () => {
   
    return ButtonOpenSectionAndClose( 'none', 'none', 'block', 'block', 'block', 'none', 'block', null);
  }

    return (
      <>
        <div className="container margin-bottom">
          <div className=" row justify-content-center">
            <div className="col-9 col-md-6">     
                <div className="multi-button">
                    <button onClick={()=> {ButtonNavDates()}} >Dates</button>
                    <button onClick={()=> {ButtonNavHistory()}} >History</button>
                    <button onClick={()=> {ButtonNavTechTree()}}>Tech tree</button>
                </div>
            </div>
          </div>                                                              
       </div>
      </>
    );
  }
  
  export default NavbarCivilizations;

