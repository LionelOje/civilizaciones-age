import React, {useState, useEffect, useContext} from 'react';
import DatesApi from './dates-api';
import { ContextModalCiviliztions } from './modal-civilizations';


//Este componente me contiene los datos de la priemera opcion en el menu modal, donde se muestran caracterizticas de la civilizacion seleccionada. Ademas me contiene el componente Dates-Api que contiene parte de esos datos y que los obtengo consumiendo una api.

const DatesOverview = ({opcion1, opcion2, opcion2img, opcion3a, opcion3b, opcion4, opcion5a, opcion5b, opcion5c, opcion5d, opcion6,  ImgShield, ImgWonder, NameWonder, NameShield, DisplayDates}) => {
    
    const {effectDescriptionAndDates} =  useContext(ContextModalCiviliztions);


    // Con este estado y el useEffect lo que hago es, que cuando la pantalla esta en tamaño mobil, no aparesca la imagen de la unidad especial de la civilizacion. No la muestro por que tamaño mobil visualmente la estetica de esta parte de la app cambia y me queda mal con otra imagen mas.

    const [DisplayImgUnity, setDisplayImgUnity] = useState('block');

    useEffect(() => {
    
        let windowSize = window.innerWidth;

        if (windowSize<700){
          setDisplayImgUnity('none');
        }
      },[])


    return (
      <>
    
        <div className="container re" style={{display:DisplayDates}}>
        <div className={effectDescriptionAndDates}>
          <div className=" row">
            <div className="col-12 col-md-7">     
                <div className="row">     
                    <p><b className="me-1">Specialty:</b>{opcion1}.</p>
                </div>
                <div className="row">
                    <div className="d-flex">     
                        <b>Unity:</b>
                        <img className="col-1 me-1 ms-1" height="24px" src={opcion2img}></img>
                        <p>{opcion2}.</p>
                    </div>
                </div>    
                <div className="row">     
                    <b>Unique technologies:</b>
                    <div className="d-flex ms-3">     
                        <img className="col-1 me-1" height="24px" src="./corona-plata.png"></img>
                        <p>{opcion3a}.</p>
                    </div>
                    <div className="d-flex ms-3">     
                        <img className="col-1 me-1" height="24px" src="./corona-oro.jpg"></img>
                        <p>{opcion3b}.</p>
                    </div>
                </div>
                <div className="row">     
                    <p><b>Wonder:</b>{opcion4}.</p>
                </div>
                <div className="row">     
                    <b>Civilization bonuses:</b>
                        <p> {opcion5a}</p>
                        <p> {opcion5b}</p>
                        <p>{opcion5c}</p>
                        <p> {opcion5d}</p>
                </div>
                <div className="row">     
                    <p><b>Team bonus:</b>{opcion6}.</p>
                </div>
                <DatesApi/>
            </div>
            <div className="col-12 col-md-5">
                <div className="row justify-content-center text-center"> 
                    <img className="col-7 col-md-9" src={ImgShield}></img>
                    <p>{NameShield}</p>
                </div> 
                <div className="row">
                    <div className="mt-5 text-center">
                        <img className=" col-md-12" src={ImgWonder}></img>
                        <p>{NameWonder}</p>
                    </div>
                </div>         
                <div className="row" style={{display:DisplayImgUnity}}>
                    <div className=" text-center" style={{marginTop:136}}>
                        <img className=" col-md-12" src={opcion2img}></img>
                        <p>{opcion2}</p>
                    </div>
                </div>
            </div>
          </div>                                                              
       </div>
       </div>


      </>
    );
  }
  
  export default DatesOverview;

