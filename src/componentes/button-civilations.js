import React, {useContext} from 'react';
import './button-civilations.css'
import {ContextBackground} from './background'


//Este componente lo utilizo en el componente glass que se situa en el home y lo utilizo para desplegar todas las civilizaciones.

const ButtonCivilations = () => {
  
  const {FunctionClickButtonGlass} = useContext(ContextBackground); 

  return (
      <>
        <div className="row  justify-content-center" onClick={()=> {FunctionClickButtonGlass()} }>
          <div className=" col-8 col-md-3 col-sm-4 btn-civilitations ">
            <img className="margin-btn-civilitations" src="./button-see.png"></img>
          </div>
        </div>
      </>
    );
  }
  
  export default ButtonCivilations;
