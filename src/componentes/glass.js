import React, {useState,useEffect} from 'react';
import './civilitations.css'
import './glass.css'
import SearchBox from './search-box/SearchBox';
import ButtonCivilations from './button-civilations';


//Este componente contiene el buscador y el boton de buscar todas las civilizaciones y esta situado en el componente home
const Glass = ({ scale, coco}) => {

  


    return (
      <>
        <div className="container" >
          <div className=" row justify-content-center">
            <div className="col-8 glass-efect" style={{transform: scale, display: coco}}>     
              <div className="row"> 
              
                <SearchBox ClassCol="col-md-10 col-sm-8 col-md-offset-3"  height="height"   />
              </div>
              <ButtonCivilations/>
            </div>
          </div>                                                              
       </div>
      </>
    );
  }
  
  export default Glass;


  /*
  import React from 'react';
import './civilitations.css'
import SearchBox from './search-box/SearchBox';
import ButtonCivilations from './button-civilations';

const Glass = () => {
    return (
      <>
        <div className="container">
          <div className=" row justify-content-center">
            <div className="col-8 glass-efect">     
              <div className="row"> 
                <SearchBox/>
              </div>
              <ButtonCivilations/>
            </div>
          </div>                                                              
       </div>
      </>
    );
  }
  
  export default Glass;





  const [topGlass, settopGlass] = useState();

  const FunctionCondicionToppGlass = (windowSizee, desde, hasta) => {

    if(windowSizee>=desde && windowSizee<=hasta ){}
  }
  
  const ToppGlass = () => {

    let windowSize = window.innerHeight;
    let desde = 660;
    let hasta = 680;
    let flagCoincidenciaTmaño = false;
    let medidaTopGlassSegunSize = 370;
    let medidaString;
    

    if (windowSize>=660){
      
      while (flagCoincidenciaTmaño===false){
        
        if(windowSize>=desde && windowSize<=hasta ){
        
          flagCoincidenciaTmaño = true;
          medidaString = medidaTopGlassSegunSize + '';
          settopGlass(medidaString +'px')
        }
        else{

          medidaTopGlassSegunSize+=20;
          desde+=20;
          hasta+=20
        }
    
      }
    }  
  }


  useEffect(() => {

    window.addEventListener('resize', ToppGlass);   
    window.addEventListener('load', ToppGlass);  

    return () =>{

      window.removeEventListener('resize', ToppGlass);
      window.removeEventListener('load', ToppGlass);
    }

  })

  */