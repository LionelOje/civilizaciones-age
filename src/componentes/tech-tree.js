import React, {useContext} from 'react';
import { ContextModalCiviliztions } from './modal-civilizations';
import './tech-tree.css'

// Este el componente del arbol tecnologico o tech tree de cada civilizacion

const TechTree = ({ImgTech, DisplayTechtree}) => {
  
    const {effectTechTree}  = useContext(ContextModalCiviliztions);

    return (
        <div className={effectTechTree} style={{display:DisplayTechtree}}>

            <img className="size-img-tech-tree"   src={ImgTech}></img>
        </div>
        );
  }
  
  export default TechTree;

