import React from 'react';
import './card-civilization.css'


// Este componente es el de la card de cada civilizacion que se se muestra en el componente container-card-civilizations

const CardCivilization = ({name, image, ButtonCard, id}) => {
    return (
      <>
        <div className="container col-12 col-sm-6 col-md-6 position" >
          <div className="carta tex-center">
            <div className="lado front">
              <img className="img-card-civi" src={image}></img>     
            </div>
            <div className="lado back ">
              <img className="img-card-civi" src="./marco4.jpg"></img>
              <img className="button-card-back col-7 col-sm-7 col-md-7"  onClick={()=> {ButtonCard(id)} }  src="./button-see.png" ></img>
            </div>
          </div>
            <h1 className="name-civilization">{name}</h1>
        </div>
      </>
    );
  }
  
  export default CardCivilization;

  
  
    
