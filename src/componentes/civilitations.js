import React, {useState, useEffect, useContext} from 'react';
import './civilitations.css'
import SearchBox from './search-box/SearchBox';
import ContainerCardCivilization from './container-card-civilization';
import ModalCivilitations from './modal-civilizations';
import ButtonCloseUndo from './button-close-undo';
import {ContextBackground} from './background'


//Creacion del context que contiene los estados y sets ubicados en este componente

export const ContextCivilitations = React.createContext(
  );


  // Esta es la segunda vista de la app, y me muestra todas las card de las civilizaciones, a partir de las cuales al hacer click me abre un modal con las caracterizticas de cada civilizacion. Contiene el componente contenedor de las cards de las civilizaciones, un componente con un buscador para buscar cada civilizacion y a lo ultimo el modal. El modal esta situado abajo de todo por que esta app no utiliza rutas, sino que voy mostrando las vistas manejando los display y el z-index

const Civilitations = ({TranslateY, DisplayCardsCivilizationWhitTimeOut, FunctionMoveElementsWhitTimeOut, FunctionSet, ChangeBackground, setMoveGlass, setMoveImgAge, setTurnOffLightCivilizations, setMovePergaCivilization, TurnOffLightCivilizations, setTurnOffLight, TurnOffLight}) => {

  const {setSearchText} = useContext(ContextBackground);



  /////////////////////////////////////////////////// Funcionalidad imagen pergamino que contiene las cards de las civilizaciones


  //Estado para manejar la visualizacion de la barra de busqueda en la imagen del pergamino
  
  const [TopSearchBox, setTopSearchBox] = useState('112px');
  
// Esta funcion me controla posicion del la barra de busqueda en la imagen del pergamino segun sea el tamaño de la pantalla. 

  const FlagResize = () => {

  let windowWidth = window.innerWidth;
  let windowHeight = window.innerHeight;
  
  if (windowWidth>=575){
    if (windowHeight>=260 && windowHeight<=330){
      setTopSearchBox('70px')
    }
    else{
      setTopSearchBox('86px')
    }
  }
  else{
    setTopSearchBox('110px');
  }

}


  useEffect(() => {

    window.addEventListener('resize', FlagResize);   
    window.addEventListener('load', FlagResize);  

    return () =>{

      window.removeEventListener('resize', FlagResize);
      window.removeEventListener('load', FlagResize);
    }

  })



 
 

  /////////////////////////////////////////////////////////////Funcionalidad del boton para volver al home


  // Funcion del boton de volver al home que esta situado en el componente civilizaciones. Funciona igual que el boton See all del home, pero hace todo a la inversa, osea vuelve todo a como estaba en el home. Es por eso que pase como prop los estados que utiliza el boton see all en el componente background
  
  const FunctionButtonComeBackHome = () => { 
  
    if(TurnOffLight="animation-turnOff-light"){
      setTurnOffLight("desactive");  
    };

    FunctionMoveElementsWhitTimeOut(setMoveGlass,"scale(1)",2500);
    FunctionMoveElementsWhitTimeOut(setMoveImgAge,"-140px",1500);
    FunctionSet(setTurnOffLightCivilizations,"animation-turnOff-light");
    ChangeBackground("background-home");
    FunctionSet(setMovePergaCivilization,"translateY(-1200px)");
    DisplayCardsCivilizationWhitTimeOut();
    setSearchText('');
  }




  //////////////////////////////////////////////////////////Estados que declare aca para pasarlos a otros componentes

  //Estado objeto que utilice a modo bandera para manejar en que card se hace click y realizar acciones segun eso tanto desde el componente container-cards-civilizaciones como en el componente modal 
  
  const [FlagsActivCivilization, setFlagsActivCivilization] = useState({
    1: false,
    2: false,
    3: false,
    4: false,
    5: false,
    6: false,
    7: false,
    8: false,
    9: false,
    10: false,
    11: false,
    12: false,
    13: false,
    14: false,
    15: false,
    16: false,
    17: false,
    18: false,
  } )
  

  //Estado objeto que contiene los display de los datos situados en el componente DatesOverview, que corresponden a cada civilizacion, y que luego voy modificando segun en que card se haga click o segun que opcion se eliga en el nav modal o cuando abro y cierro el modal
  
  const [DisplayDates, setDisplayDates] = useState({
    1: 'none',
    2: 'none',
    3: 'none',
    4: 'none',
    5: 'none',
    6: 'none',
    7: 'none',
    8: 'none',
    9: 'none',
    10: 'none',
    11: 'none',
    12: 'none',
    13: 'none',
    14: 'none',
    15: 'none',
    16: 'none',
    17: 'none',
    18: 'none',
  })
 

  //Estados para manejar la vista del componente modal, el display y el z-index para superponer la vista del componente ante los otros
  
  const [DisplayModal, setDisplayModal] = useState(null);
  const [ZindexModal, setZindexModal] = useState(null);

  
  //Estado para manejar el display del componente civilizacion cuando abro y cierro el modal
  
  const [DisplayCivilization, setDisplayCivilization] = useState(null);


  //Estado objeto para manejar los datos que recibo del api y que se reflejen en el componente DatesApi. Lo declare aca por que lo utiliza el componente container-card-civilization
  
  const [DatessApi, setDatessApi] = useState({
    name: null,
    first_atack: null,
    second_atack: null,
    creation_time: null,
    armor_level: null,
    damage_: null,
    strong_against: null,
  } )


  //  <ButtonCloseUndo Action={FunctionButtonComeBackHome}/>
  
   //voy a pasar como prop desde el componente search
   return (
      <>
        <div className={TurnOffLightCivilizations}></div>
       
        <ContextCivilitations.Provider value={{FlagsActivCivilization, setFlagsActivCivilization, DatessApi, setDisplayCivilization, setDisplayModal, setZindexModal, setDisplayDates, DisplayDates}}>
         
        <div className="container transformY" style={{transform:TranslateY,display:DisplayCivilization}}>
  
          <div className="row  justify-content-center">
            <div className="d-flex justify-content-center">
              <img className="col-9 perga-civilitations" src='./pergamino-overwiev.jpg'></img>
              <SearchBox PositionMobileHorizontal="PositionMobileHorizontal"  ClassCol="col-5 col-md-3 " position="absolute" top={TopSearchBox} BorderRadiusLeft1="2em" BorderRadiusLeft2="2em"/>
            </div>
            <ContainerCardCivilization setDatessApi={setDatessApi} />
          </div>
        </div>
        <ModalCivilitations DisplayModal={DisplayModal} ZindexModal={ZindexModal} />
      
        </ContextCivilitations.Provider>

      </>
    );
  }
  
  export default Civilitations;