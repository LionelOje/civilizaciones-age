import React, {useState, useContext} from 'react';
import GroupTechTree from './group-tech-tree';
import './modal-civilizations.css'
import NavbarCivilizations from './navbar-civilizations';
import Overview from './overview';
import ButtonCloseUndo from './button-close-undo'
import {ContextCivilitations} from './civilitations'
import { ContextBackground } from './background';


export const ContextModalCiviliztions = React.createContext(
  );
  

//Este componente me contiene el modal al cual se accede haciendo click en el boton que esta dentro de cada card de las civilizaciones. Dentro del modal tengo un menu donde donde cada opcion me muestra distintas caracterizticas de cada civilizacion.

const ModalCivilitations = ({DisplayModal, ZindexModal}) => {

  const {setClassBackground} = useContext(ContextBackground)

  const {FlagsActivCivilization, setFlagsActivCivilization, setDisplayCivilization, setDisplayModal, setZindexModal, setDisplayDates, DisplayDates} = useContext(ContextCivilitations)




  ///////////////////////////////////////////////////Estados para manejar los displays de las historias y los arboles tecnologics de cada civilizacion


  //Estado objeto que contiene los display de las descripciones de las civilizciones (la historia) situados en el componente DescriptionOverview, y que luego voy modificando segun en que card se haga click, o segun que opcion se eliga en el nav modal o cuando abro y cierro el modal

  const [DisplayDescriptions, setDisplayDescriptions] = useState({
   1: 'none',
   2: 'none',
   3: 'none',
   4: 'none',
   5: 'none',
   6: 'none',
   7: 'none',
   8: 'none',
   9: 'none',
   10: 'none',
   11: 'none',
   12: 'none',
   13: 'none',
   14: 'none',
   15: 'none',
   16: 'none',
   17: 'none',
   18: 'none',
  })
  
  
  //Lo mismo que el estado objeto de arriba, con la diferencia de que esto son los displays que corresponden a la tercer pestaña del nav que se encuentra en el modal y que corresponde a las imagenes con los arboles tecnologicos de cada civilizacion

  const [DisplayTechTrees, setDisplayTechTrees] = useState({
   1: 'none',
   2: 'none',
   3: 'none',
   4: 'none',
   5: 'none',
   6: 'none',
   7: 'none',
   8: 'none',
   9: 'none',
   10: 'none',
   11: 'none',
   12: 'none',
   13: 'none',
   14: 'none',
   15: 'none',
   16: 'none',
   17: 'none',
   18: 'none',
  })




  ///////////////////////////////////////////////////////Funcionlidad para volver a la vista del  componente Civilization


  // Esta funcion la utilizo dentro de la funcion del boton para volver a la vista del componente Civilization, y lo que hace es ponerme en por defecto, o volver a como estaban antes de haber ingresa al modal, a todos los estados que se modificaron en lo que concierne a la navegacion dentro del modal. Para ello utilize un ciclo for para detectar que flag (la civilizcion en la que se hizo click) esta en tru y cambiar todo los datos de esa civilizacion a la normalidad

  const DesactiveElements = () =>{
    
    for (let i=1; i<=18; i++){
      if (FlagsActivCivilization[i]===true){
          setFlagsActivCivilization({...FlagsActivCivilization, [i]:false})
          setDisplayDates({...DisplayDates, [i]:'none'})
          setDisplayContainerGroupDates('block')
          setDisplayOverview('block') 
          setDisplayDescriptions({...DisplayDescriptions, [i]:'none'})
          setDisplayContainerGroupDescription('none')
          setDisplayTechTrees({...DisplayTechTrees, [i]:'none'})
      }      
    }
  }

  
  // Esta funcion es la contraria a la funcion ActiveModal que se encuentra en el componente civilization. Me cierra La ventana modal  

  const DesactiveModal = (Modal, Index, Background, display) =>{
   
    setDisplayModal(Modal)
    setZindexModal(Index)
    setClassBackground(Background)
    setDisplayCivilization(display)
  }


  // Esta funcion corresponde al boton close-undo, y lo que hace es llamar a las dos funciones anteriores para ponerme los valores de los elementos con los que interactue a por defecto y desactivarme la ventana modal.

  const ButtonCloseModal = () => {
    DesactiveElements()
    DesactiveModal('none', 0, 'background-civilizations','block')
  } 




/////////////////////////////////////////////////////////////////////// Estados que utilizo para los display y los efectos al cambiar de pestaña en el nav del modal


  // Estado para mostrar y ocultar el contenedor de los dates-descriptions (historias de las civilizaciones) cuando selecciono la pestaña history del modal o, cuando abro y cierro el mismo
  const [DisplayContainerGroupDescription, setDisplayContainerGroupDescription] = useState('none')

  //Estado para el efecto de aparecer de la seccion dates-descriptions del modal, con la propiedad opacity 
  const [effectDescriptionAndDates, seteffectDescriptionAndDates] = useState(null)

  //Estado para el efecto de aparecer de la seeccion tech tree del modal, con la propiedad transform scale
  const [effectTechTree, seteffectTechTree] = useState(null)

  // Estado para mostrar y ocultar el contenedor de los dates-overview (datos de las civilizaciones) cuando selecciono la pestaña dates del modal o, cuando abro o cierro el mismo 
  const [DisplayContainerGroupDates, setDisplayContainerGroupDates] = useState('block')

  //// Estado para mostrar y ocultar el contenedor de los datos e historias de las civilizaciones cuando elijo esas pestañas o cuando abro y cierro el modal 
  const [DisplayOverview, setDisplayOverview] = useState('block')

//<ButtonCloseUndo Action={ButtonCloseModal} />
  return (
    <>            
       <ContextModalCiviliztions.Provider value={{DisplayDates, DisplayTechTrees, DisplayDescriptions, DisplayContainerGroupDescription, DisplayContainerGroupDates, effectDescriptionAndDates, effectTechTree}}>

       <div className="modal l" style={{display:DisplayModal, zIndex:ZindexModal}}>
                          
             <NavbarCivilizations setDisplayDates={setDisplayDates} setDisplayDescriptions={setDisplayDescriptions} setDisplayTechTrees={setDisplayTechTrees} setDisplayContainerGroupDescription={setDisplayContainerGroupDescription} setDisplayContainerGroupDates={setDisplayContainerGroupDates} seteffectDescriptionAndDates={seteffectDescriptionAndDates} setDisplayOverview={setDisplayOverview} seteffectTechTree={seteffectTechTree} />
             <Overview DisplayOverview={DisplayOverview} />
             <GroupTechTree />    
       </div>

       </ContextModalCiviliztions.Provider>
    </>
  );
}

export default ModalCivilitations;


