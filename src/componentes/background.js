import React, {useState} from 'react';
import './background.css'
import Glass from './glass';
import Civilitations from './civilitations';


//doc. Creacion del context que contiene los estados y sets ubicados en este componente
export const ContextBackground = React.createContext(
);


//Este es el componente principal de la app. Apenas se carga me muestra este componente en primera estancia. Contiene al componente glass, donde estan el buscador y el boton de ver todas las civilizaciones y al componente civilizciones, que es la segunda vista que se renderisa al buscar una civilizaciones atraves del boton search del buscador o del boton see All

const Background = () => {
  
  // Estados para cambiar de posicion los elementos del home, la imagen de pergamino de el componente civilizations, las clases para activar los keyframes del efecto de oscurecer la pantalla y la clase para cambiar de fondo

  const [MoveImgAge, setMoveImgAge] = useState(null);
  const [MoveGlass, setMoveGlass] = useState(null);
  const [MovePergaCivilization, setMovePergaCivilization] = useState("translateY(-1200px)");

  let [TurnOffLight, setTurnOffLight] = useState("desactive");
  let [TurnOffLightCivilizations, setTurnOffLightCivilizations] = useState("desactive");

  const [ClassBackground, setClassBackground] = useState("background-home");
  const [coco, setcoco] = useState(null);




  //////////////////////////////////////////////  FUNCIONES AUXILIARES


  // funcion para cambiar estados 

  const FunctionSet = (set,argument) => { 
    
    return set(argument);
  }
  

  /* funcion para cambiar las clases de los fondos con un lapso de tiempo usando el metodo setTimeout
  BackgroundName: el nombre de la clase que contiene el fondo */

  const ChangeBackground = (BackgroundName) =>{ 
    
    return setTimeout(function(){ setClassBackground(BackgroundName); }, 1500);
  }


   // funcion para cambiar estados de elementos que requieren un setTime

  const FunctionMoveElementsWhitTimeOut = (set,propiety,time) =>{
  
    return setTimeout(function(){ set(propiety); }, time);
  }




  //////////////////////////////////////////////////// Funcionalidad de la visualizacion de las cards de las civilizaciones que se situn en el componente container-card-civilizations 


  //Estados que funcionan a modo contador, para activar el display de las cards de las civilizaciones 
  
  const [ActivDisplayCardsCivilizationActive, setActivDisplayCardsCivilizationActive] = useState(0);
  const [ActivDisplayCardsCivilizationDesactive, setActivDisplayCardsCivilizationDesactive] = useState(0);
  

  //Funcion que me pone el estado anterior en 1 y me dispara un useEffect que se situa en el componente container-card-civilizations, ese useEffect me pone en dislay block a las cards de las civilizaciones 
  
  const DisplayCardsCivilizationActive = () => {

   return setActivDisplayCardsCivilizationActive(ActivDisplayCardsCivilizationActive +1);
  }


  //Esta funcion hace lo mismo quela anterior, pero a la inversa con un useEffect que me pone las card de las civilizaciones en display none
  
  const DisplayCardsCivilizationDesactive = () => { 

    return setActivDisplayCardsCivilizationDesactive(ActivDisplayCardsCivilizationDesactive +1);
  }


  // Lo mismo que la anterior, pero esta ves se ejecuta luego de 2.5 seg y me pone los displays en none. Se ejecuta en 2.5 seg por que la vista que contiene las cards desaparece progresivamente, que es cuando aprieto el boton de regresar al home
  
  const DisplayCardsCivilizationWhitTimeOut = () =>{ 
    
    return setTimeout(function(){DisplayCardsCivilizationDesactive() }, 2500);
  }

 
    

///////////////////////////////// Funcionalidad  ButtonSearch


//Estado del value del imput del componente SearchBox, contiene lo que el usuario va escribiendo

const [SearchText, setSearchText] = useState('');

//Estado que me activa un useEffect que se situa en el componente container-card-civilization y que me valida si lo que escribe el usuario corresponde al nombre de alguna civilizacion

const [ActivButtonSearch, setActivButtonSearch] = useState(0);


//Funcion del botton buscar del buscador 

const ButtonSearch = () => {

  //Me activa el useEffecte que mencione arriba
  setActivButtonSearch(ActivButtonSearch +1);


  if(TurnOffLightCivilizations="animation-turnOff-light"){ 
   
    setTurnOffLightCivilizations("desactive");  // Este condicional lo hice para que me resetee la clase que use para activar los keyframes del efecto de oscurecer, que se activa cuando paso del home al componente civilizations y viseversa. De esta forma se puede usar infinitamente.
  }
  
  
  //El boton me mueve elementos, me cambia el fondo, me pone el efecto oscurecer y me pone en display block todas las cards de civilizaciones. Osea me cambia la vista al componente civilizaciones
  FunctionSet(setMoveGlass,"scale(0)");
  FunctionSet(setMoveImgAge,"-600px");
  ChangeBackground("background-civilizations");
  FunctionMoveElementsWhitTimeOut(setMovePergaCivilization,"translateY(0)",2000);
  DisplayCardsCivilizationActive();
  FunctionSet(setTurnOffLight,"animation-turnOff-light");
  
}




  ////////////////////////////////////////////Funcionalidad del boton see all que ese encuentra en el componente glass

  const FunctionClickButtonGlass = () => { 

    // Este condicional lo hice para que me resetee la clase que use para activar los keyframes del efecto de oscurecer. Asi se puede usar infinitamente. Funciona de la misma manera que en el botton search del buscador
    if(TurnOffLightCivilizations="animation-turnOff-light"){ 
      setTurnOffLightCivilizations("desactive");  
    }
    
    
    //el boton me mueve elementos, me cambia el fondo, me pone el efecto oscurecer y me pone en display block todas las cards de civilizaciones. Osea me cambia la vista al componente civilizaciones
    FunctionSet(setMoveGlass,"scale(0)");
    FunctionSet(setMoveImgAge,"-600px");
    ChangeBackground("background-civilizations");
    FunctionMoveElementsWhitTimeOut(setMovePergaCivilization,"translateY(0)",2000);
    DisplayCardsCivilizationActive();
    FunctionSet(setTurnOffLight,"animation-turnOff-light");
    setSearchText('')
  }


  return (
    <>
      <div className={ClassBackground}>
       <div className="img-age" style={{top:MoveImgAge}}></div>
       <div className={TurnOffLight}></div>

       <ContextBackground.Provider value={{FunctionClickButtonGlass, SearchText, ButtonSearch, setSearchText, ActivDisplayCardsCivilizationDesactive, ActivDisplayCardsCivilizationActive, ActivButtonSearch, setClassBackground}}> 

       <Glass coco={coco}  scale={MoveGlass}  />
       <Civilitations DisplayCardsCivilizationWhitTimeOut={DisplayCardsCivilizationWhitTimeOut} FunctionSet={FunctionSet} ChangeBackground={ChangeBackground} FunctionMoveElementsWhitTimeOut={FunctionMoveElementsWhitTimeOut} setMoveGlass={setMoveGlass} setMoveImgAge={setMoveImgAge} setMovePergaCivilization={setMovePergaCivilization} setTurnOffLightCivilizations={setTurnOffLightCivilizations} TurnOffLight={TurnOffLight} setTurnOffLight={setTurnOffLight}
       TranslateY={MovePergaCivilization}  TurnOffLightCivilizations={TurnOffLightCivilizations} />
      
       </ContextBackground.Provider>

      </div>
    </> 
  );
}

export default Background;

