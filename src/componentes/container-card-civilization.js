import React, {useState, useEffect, useContext} from 'react';
import './container-card-civilization.css'
import CardCivilization from './card-civilization';
import {ContextBackground} from './background'
import {ContextCivilitations} from './civilitations'


// Este componente me sirve como contenedor para las cards de las civilizaciones

const ContainerCardCivilization = ({setDatessApi}) => {
  
  const {SearchText, setSearchText, ActivDisplayCardsCivilizationActive, ActivDisplayCardsCivilizationDesactive, ActivButtonSearch, setClassBackground} = useContext(ContextBackground);
  
  const {FlagsActivCivilization, setFlagsActivCivilization, setDisplayCivilization, setDisplayModal, setZindexModal, DisplayDates, setDisplayDates, DatessApi} = useContext(ContextCivilitations);


  // Esta funcion me obtiene los datos de la api y los manda al componente datesApi  atraves del setDatessApi
 
  const GetDatesUnity = async (untityNumber, namee) =>{
   
    const response = await fetch( 'https://pokeapi.co/api/v2/pokemon/'+untityNumber);
    const datesUnity = await response.json();
   
    setDatessApi({...DatessApi,
    name: namee,
    first_atack: datesUnity.abilities[0].ability.name,
    second_atack: datesUnity.abilities[1].ability.name,
    creation_time: datesUnity.weight,
    armor_level: datesUnity.height,
    damage: datesUnity.base_experience,
    strong_against: datesUnity.moves[0].move.name
    }); 
  }




  ///////////////////////////////////////////////////////// Funcionalidad buttonCard
  
  //Funcion que me activa el modal cuando hago click en algun card de civilizacion. Me pone el display del modal en block, me pone un numero de z index mayor a las demas vistas, me cambia la clase del fondo y me pone el display del componente civilizacion en none
 
  const ActiveModal = (Modal, Index, Background, display) =>{
    
    setDisplayModal(Modal);
    setZindexModal(Index);
    setClassBackground(Background);
    setDisplayCivilization(display);
  }


  //Funcion que me pone el display de la seccion dates de cada civilizcion en block ( se encuentra en el modal) y me pone la bandera que corresponde a esa civilizacion en true, para usarla como una referencia en otras acciones con respecto a es civilizacion. El i del parametro  corresponde al numero de civilizcion  ya que esta funcion la llamo en el swicht de abajo y uso el parmetro como referencia de civilizacion
  
  const ActiveDates = (i) => {
    
    setDisplayDates({...DisplayDates, [i]:'block'});
    setFlagsActivCivilization({...FlagsActivCivilization, [i]:true});
  } 
 

  //Esta  funcion corresponde al boton de cada card. Me activa el modal, y segun en que card se hizo click me ejecuta la funcion ActiveDates, que se encuetra declarada aca arriba. Ademas me hace el llamado a la api y Utiliza como parametro el id que corresponde a cada card para saber en cual se hizo click
  const ButtonCard = (id) =>{
    
    ActiveModal('block', 10000000, 'background-modal', 'none');
      
    switch (id) {
      case 1:      
        GetDatesUnity('1', 'Longbowman');      
        ActiveDates(1);
        break;
      case 2:
        GetDatesUnity('2', 'Cataphract');
        ActiveDates(2);
        break;
      case 3:
        GetDatesUnity('3', 'Woad Raider');
        ActiveDates(3);
        break;
      case 4:
        GetDatesUnity('4', 'Chu Ko Nu');
        ActiveDates(4);
        break;
      case 5:
        GetDatesUnity('5', 'Throwing Axeman');
        ActiveDates(5);
        break;
      case 6:
        GetDatesUnity('6', 'Huskarl');
        ActiveDates(6);
        break;
      case 7:
        GetDatesUnity('7', 'Samurai');
        ActiveDates(7);
        break;
      case 8:
        GetDatesUnity('8', 'Mangudai');
        ActiveDates(8);
        break;
      case 9:
        GetDatesUnity('9', 'Mameluke');
        ActiveDates(9);
        break;
      case 10:
        GetDatesUnity('10', 'War Elephant');
        ActiveDates(10);
        break;
      case 11:
        GetDatesUnity('11', 'Teutonic Knight');
        ActiveDates(11);
        break;
      case 12:
        GetDatesUnity('12', 'Janissary');
        ActiveDates(12);
        break;
      case 13:
        GetDatesUnity('13', 'Berserk');
        ActiveDates(13);
        break;
      case 14:
        GetDatesUnity('14', 'Jaguar Warrior');
        ActiveDates(14);
        break;
      case 15:
        GetDatesUnity('15', 'Tarkan');
        ActiveDates(15);
        break;
      case 16:
        GetDatesUnity('16', 'War wagon');
        ActiveDates(16);
        break;
      case 17:
        GetDatesUnity('17', 'Plumed Archer');
        ActiveDates(17);
        break;
      case 18:
        GetDatesUnity('18', 'Conquistador');
        ActiveDates(18);
        break;                                                     
      default:
        break;
    }
  } 


// Lista con los datos visuales de cada card de las civilizaciones
 
const CardDatos = [
    {
      id:1,
      image:'./ingleses.jpeg',
      name:'ingleses', 
      ButtonCard: ButtonCard,
    },
    {
      id:2,
      image:'./bizantinos.jpg',
      name:'bizantinos',
      ButtonCard: ButtonCard,
    },
    {
      id:3,
      image:'./celtas.jpg',
      name:'celtas',
      ButtonCard: ButtonCard, 
    },
    {
      id:4,
      image:'./Chinos.png',
      name:'chinos',
      ButtonCard: ButtonCard,
    },
    {
      id:5,
      image:'./franceses.jpeg',
      name:'franceses',
      ButtonCard: ButtonCard,
    },
    {
      id:6,
      image:'./godos2.jpg',
      name:'godos',
      ButtonCard: ButtonCard,
    },
    {
      id:7,
      image:'./japoneses.jpg',
      name:'japoneses',
      ButtonCard: ButtonCard,
    },
    {
      id:8,
      image:'./mongoles.jpg',
      name:'mongoles',
      ButtonCard: ButtonCard,
    },
    {
      id:9,
      image:'./serrasenos.jpg',
      name:'serracenos',
      ButtonCard: ButtonCard,
    },
    {
      id:10,
      image:'./persas.jpg',
      name:'persas',
      ButtonCard: ButtonCard, 
    },
    {
      id:11,
      image:'./teutones.jpg',
      name:'teutones',
      ButtonCard: ButtonCard, 
    },
    {
      id:12,
      image:'./turcos}.jpg',
      name:'turcos',
      ButtonCard: ButtonCard, 
    },
    {
      id:13,
      image:'./vikingos.jpg',
      name:'vikingos',
      ButtonCard: ButtonCard, 
    },
    {
      id:14,
      image:'./aztecas.jpg',
      name:'aztecas',
      ButtonCard: ButtonCard, 
    },
    {
      id:15,
      image:'./hunos.jpg',
      name:'hunos',
      ButtonCard: ButtonCard, 
    },
    {
      id:16,
      image:'./coreanos.jpg',
      name:'coreanos',
      ButtonCard: ButtonCard, 
    },
    {
      id:17,
      image:'./Mayas.jpg',
      name:'mayas',
      ButtonCard: ButtonCard, 
    },
    {
      id:18,
      image:'./españoles.jpg',
      name:'españoles',
      ButtonCard: ButtonCard, 
    },
  ];




  ///////////////////////////////////////////////////////////////Activar y desactivar visualizacion de cards civilizaciones

  //Estado que uso para activar la visualizcion de las cards de las civilizaciones
  const [Cards, setCards] = useState([]);


  // Este useEffect se ejecuta cuando se hace click en los botones See all y el de buscar del buscador que se encuentran en el home, y son para activar la visualizacion de las cards de las civilizaciones. Lo hace manipulando el nombre de la lista de datos que se utiliza en el map de abajo 
  useEffect(() => {
   
    setCards(CardDatos);

  }, [ActivDisplayCardsCivilizationActive]);


 // Este useEffect me hace lo mismo que el anterior, pero este me desactiva la visualizacion de las cards, y lo hice utilizando un filter que me genere una nueva lista con los objetos del array que contengan como name michi, que no lo tiene ninguno
  
 useEffect(() => {
   
  const Cardss = CardDatos.filter((cardsss) => cardsss.name === 'michi' );
  
  setCards(Cardss);
  
}, [ActivDisplayCardsCivilizationDesactive])




  //////////////////////////////////////////////////////////////Funcionalidad de la validacion del imput del buscador del boton search 

  //Este useEffect se ejecuta cuando el estado ActivButtonSearch es distinto de 0. Osea cuando el usuario hace click en el boton search del buscador. Me pasaba que se ejecutaba apenas se renderesiba la app, por eso le puse el distinto de 0 a modo que se active cuando se haga click y no cuando se renderisa al inicio. Ademas, me pasa lo que el usuario escriba a minuscula y luego busca con un filter, si hay en la lista de cards alguna civilizacion que conicida con el nombre que escribio el usuario. Si no hay ninguna coincidencia me tira un alert avisando, y si la hay me pone como nombre de la lista que se ejecut abajo, al nombre del arreglo generado con las coicidencias entre lo que escribio el usuario en el buscador y los nombres de las civilizaciones 

  useEffect(() => {

    if(ActivButtonSearch!=0) {
    
      let VariableSearchText = SearchText;
      VariableSearchText = VariableSearchText.toLowerCase();
      const CivilizationSearch = CardDatos.filter((cardsss) => cardsss.name === VariableSearchText);

    if(CivilizationSearch.length === 0){
      setSearchText('');
      
      alert("There is not civilization whit that name");
    }
    else{
      setCards(CivilizationSearch);
      setSearchText('');
    }
     }  
     
  }, [ActivButtonSearch])

  
  return (
      <>
        <div className="container container-position">
          <div className="row">
            <div className="col-7 col-md-6 container-cards">
            { 
             Cards.map(({ id, image, name, ButtonCard}) => <CardCivilization id={id} image={image} name={name} ButtonCard={ButtonCard} />)
            }
            </div>
          </div>
        </div>
       
      </>
    );
  }
  
  export default ContainerCardCivilization;


  