import React, {useContext} from 'react';
import {ContextCivilitations} from './civilitations'


//Este componente me contiene los datos que obtengo de la api, y se situa dentro del componente card-civilzation

const DatesApi = () => {
  
  const {DatessApi} = useContext(ContextCivilitations);
  
  return (
      <>
        <div className="row">
            <b className="mb-2">Special unit features:</b>
            <p>- Name: {DatessApi.name}.</p>
            <p>- First Atack: {DatessApi.first_atack}.</p>
            <p>- Second Atack: {DatessApi.second_atack}.</p>
            <p>- Creation time: {DatessApi.creation_time} seg.</p>
            <p>- Armor level: {DatessApi.armor_level}+.</p>
            <p>- Damage: {DatessApi.damage}+.</p>
            <p>- Strong against: {DatessApi.strong_against}.</p>
        </div>
      </>
    );
  }
  
  export default DatesApi;

