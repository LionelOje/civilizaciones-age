import React, {useContext} from 'react';
import {ContextModalCiviliztions} from './modal-civilizations'


//Este componente me contiene la historia de cada ciivilizacion que se muestra en la segunda pestaña del modal.

const DescriptionOverview = ({text, DisplayDescription}) => {

  const {effectDescriptionAndDates} = useContext(ContextModalCiviliztions);

    return (
      <>
        <div className='container' style={{display:DisplayDescription}}>
          <div className=" row justify-content-center">
            <div className="col-12 col-md-12"  >     
                <p className= {effectDescriptionAndDates}>{text}</p>
            </div>
            
          </div>                                                              
       </div>
      </>
    );
  }
  
  export default DescriptionOverview;