import React,{useContext} from 'react';
import './search-box.css'
import {ContextBackground} from '../background'


//Este componente contiene el buscador de las civilizaciones.Lo utilizo en el home y dentro de la vista civilizaciones

const SearchBox = ({a,  ClassCol, position, top, BorderRadiusLeft1, BorderRadiusLeft2, }) => {
    
  const {SearchText, ButtonSearch, setSearchText} = useContext(ContextBackground);

  //Esta funcion es del evento onchange, que se encarga de ir pasandole al imput lo que el usuario escribe en el buscador y ademas, guarda lo que se escribe dentro del estado searchText, para que lo puedo utilizar en otras partes de la app.

  const InputChange = (e) =>{
 
    setSearchText(e.target.value);
   }


   // Esta funcion es para que cuando presione la tecla enter me busque la civilzacion

   const SearchWhenPressEnter = (e) =>{
 
    if (e.key === 'Enter'){
      e.preventDefault();
      ButtonSearch();
    }
   }

  return (
      <>
<div className="container d-flex justify-content-center MarginTop" style={{position:position, top:top, display:a}} >
  <div className={ClassCol} >     
    <div className="row">
      <form role="form" id="form-buscar">
        <div className="form-group">
          <div className="input-group">
            <input id="1" className="form-control border-input" value={SearchText} onChange={InputChange} onKeyPress={SearchWhenPressEnter} type="text" name="search" placeholder="Search civilitation..." required style={{borderTopLeftRadius:BorderRadiusLeft1, borderBottomLeftRadius:BorderRadiusLeft2}}/>
            <span className="input-group-btn">
              <div className="btn  color-btn border-radius-btn " type="submit">
                <img className="search-icon" onClick={()=> {ButtonSearch()} } src="./search-icon.png"></img>
              </div>
            </span>
          </div>
        </div>
      </form>
    </div>            
  </div>
</div>
      </>
    );
  }
  
  export default SearchBox;

 